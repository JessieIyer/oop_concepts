package com.example;

// The created class is limited only to a single person ,let us make it more generalized by refactoring class name and object creation

/* public class Jessie {
	static int weight = 65;

	static void walk() {
		System.out.println("Jessie decides to lose weight");
		weight--;
	}

	// static methods can be used even without instance of a class
	static void eatWholePizza() {
		if (PizzaBox.isEmpty) {
			System.out.println("No pizza to eat");
			return;
		}
		System.out.println("Jessie is munching on whole pizza");
		weight++;
	}
} */

// AFTER REFACTORING
public class Person {

	public Person() {
		System.out.println("Default constructor call");
	}

	public Person(int weight) {
		this.weight = weight;
		System.out.println("In parametrized constructor");
		System.out.println("Person2 weight = " + weight);
	}

	private int weight;
	// static int weight = 65;

	// static void walk()
	void walk() {
		System.out.println("Decides to lose weight");
		weight--;
	}

	// static methods can be used even without instance of a class

	// static void eatWholeFood()
	void eatWholeFood(FoodBox food1) {
		if (food1.isEmpty) {
			System.out.println("No food to eat");
			return;
		}
		System.out.println("Binging on food!");
		weight++;
	}
}