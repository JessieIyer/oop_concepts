package com.example;

/*
 * Run configurations - VM Argument => -verbose, gives log ; Ctrl+F helps in finding the specified classes/packages in console
 */

public class OOPMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		for (String string : args) {
			System.out.println(string);
		}
		Person person1 = new Person();
		Person person2 = new Person(65);
		FoodBox food1 = new FoodBox();
		food1.food = "Dosa";

		person1.eatWholeFood(food1);
		person1.walk();

		person2.eatWholeFood(food1);
		person2.walk();

		System.out.println("The item of the day is : " + food1.food);
		// System.out.println(Person.weight);

		/*
		 * System.out.println("Jessie's current weight : " + Person.weight);
		 * Person.eatWholeFood();
		 * 
		 * System.out.println("Jessie's weight after binging on a pizza: " +
		 * Person.weight); System.out.println("Jessie decides to go for a morning jog");
		 * Person.walk();
		 * 
		 * System.out.println("Jessie's weight after 1 hour of morning jog : " +
		 * Person.weight);
		 * System.out.println("Jessie decides to workout for an hour in the evening");
		 * Person.walk();
		 * 
		 * System.out.println("Jessie's weight after lifting weights: " +
		 * Person.weight); System.out.println("Jessie falls for pizza again");
		 * Person.eatWholeFood();
		 * 
		 * System.out.println("Jessie's weight after finishing one more pizza : " +
		 * Person.weight);
		 * System.out.println("Jessie determines to opt for cross fitness");
		 * Person.walk(); Person.walk(); Person.walk();
		 * 
		 * System.out.println("Jessie's final weight : " + Person.weight);
		 */

	}

}
